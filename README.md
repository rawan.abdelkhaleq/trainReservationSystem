# Railway Reservation System

Designing an Online Railway ticket booking, reservation and cancellation system.<br>
The main aim of the project is to develop a website which would facilitate the reservation of online train tickets by appling the principles of Agile Method and other SWE Concepts.

Technologies used:

Front end: HTML, CSS, JavaScript<br>
Back end: PHP, MySQL Database

---

# Purpose

Delivered to SWE course @ Nile University Fall2022.<br>

---

# Development setup

### 1. Retrieve our project

### 2. Move project folder to xampp/htdocs folder

### 3. Restore Database

   * Goto phpMyAdmin and select Import.
   * Find 'File to import:' section and choose the file 'railway_reservation.sql' which is located under project folder and hit GO.
   
### 4.Setup Database Configurations

  * Goto project folder -> source code -> database.inc.php.
    * setup your configuratons related to MySQL.
  
 ### 5. Start Server
  * start the server and run;
    * http://localhost:8888/Folder_name/index.php (replace the port number 8888 to your port).
    
