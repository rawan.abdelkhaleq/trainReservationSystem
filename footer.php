﻿</div>
<footer class="footer">
  <div class="row d-flex justify-content-center align-items-center">
    <div class="col-6">
      <div class="d-sm-flex justify-content-center align-items-center justify-content-sm-between">
        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Fall 2022 -<?php $yr = date('F Y');  echo $yr; ?>. Project for the SWE Course - Nile University.</span>
      </div>
    </div>
  </div>
</footer>
</div>
</div>
</div>
<!-- container-scroller -->

<!-- plugins:js -->
<script src="assets/js/vendor.bundle.base.js"></script>
<script src="assets/js/jquery.dataTables.js"></script>
<script src="assets/js/dataTables.bootstrap4.js"></script>

<!-- Plugin js for this page -->
<script src="assets/js/Chart.min.js"></script>
<script src="assets/js/bootstrap-datepicker.min.js"></script>

<!-- inject:js -->
<script src="assets/js/off-canvas.js"></script>
<script src="assets/js/hoverable-collapse.js"></script>
<script src="assets/js/template.js"></script>
<script src="assets/js/settings.js"></script>
<script src="assets/js/todolist.js"></script>

<!-- Custom js for this page-->
<script src="assets/js/dashboard.js"></script>
<script src="assets/js/data-table.js"></script>
</body>

</html>