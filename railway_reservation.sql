CREATE DATABASE railway;

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


--
-- Database: `railway_reservation`
--

-- --------------------------------------------------------


-- Table structure for table `cancel_details`

Use railway;
CREATE TABLE `cancel_details` (
  `ticket_id` varchar(10) NOT NULL,
  `cancel_date` varchar(10) NOT NULL,
  `cancel_time` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table `cancel_details`


INSERT INTO `cancel_details` (`ticket_id`, `cancel_date`, `cancel_time`) VALUES
('790', '02 Jan', '03:22:59');

-- --------------------------------------------------------

-- Table structure for table `coach_price`


CREATE TABLE `coach_price` (
  `coach_type` varchar(10) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `coach_price`
--

INSERT INTO `coach_price` (`coach_type`, `price`) VALUES
('AC', 1300),
('First Class', 550),
('GENERAL', 450),
('NON-AC', 850),
('Second Class', 2050);

-- --------------------------------------------------------

-- Table structure for table `passenger_details`

CREATE TABLE `passenger_details` (
  `passenger_id` varchar(10) NOT NULL,
  `name` varchar(35) NOT NULL,
  `age` varchar(11) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `seat_no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table `passenger_details`

INSERT INTO `passenger_details` (`passenger_id`, `name`, `age`, `gender`, `seat_no`) VALUES
('26249', 'Ahmed', '19', 'male', 16),
('97484', 'Mona', '23', 'female', 70);

-- --------------------------------------------------------

-- Table structure for table `passenger_ticket`


CREATE TABLE `passenger_ticket` (
  `passenger_id` varchar(10) NOT NULL,
  `ticket_id` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table `passenger_ticket`


INSERT INTO `passenger_ticket` (`passenger_id`, `ticket_id`) VALUES
('26249', '790'),
('97484', '527');

-- --------------------------------------------------------

-- Table structure for table `station`

CREATE TABLE `station` (
  `station_id` varchar(10) NOT NULL,
  `sname` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `station`
--

INSERT INTO `station` (`station_id`, `sname`) VALUES
('S0001', 'Cairo'),
('S0002', 'Giza'),
('S0003', 'Alexandria'),
('S0004', 'Mansoura'),
('S0005', 'Tanta'),
('S0006', 'Assiut'),
('S0007', 'Aswan'),
('S0008', 'Beheira'),
('S0009', 'Bani Suef'),
('S0010', 'Daqahliya'),
('S0011', 'Damietta'),
('S0012', 'Fayyoum'),
('S0013', 'Luxor'),
('S0014', 'Ismailia');

-- --------------------------------------------------------

-- Table structure for table `ticket_amount`

CREATE TABLE `ticket_amount` (
  `ticket_id` int(11) NOT NULL,
  `total_amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table `ticket_amount`

INSERT INTO `ticket_amount` (`ticket_id`, `total_amount`) VALUES
(790, 2050),
(527, 1300);

-- --------------------------------------------------------


-- Table structure for table `ticket_details`

CREATE TABLE `ticket_details` (
  `ticket_id` int(11) NOT NULL,
  `train_no` varchar(10) NOT NULL,
  `dept_date` date NOT NULL,
  `coach_type` varchar(10) NOT NULL,
  `user_id` varchar(10) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table `ticket_details`

INSERT INTO `ticket_details` (`ticket_id`, `train_no`, `dept_date`, `coach_type`, `user_id`, `status`) VALUES
(790, 'TR016', '2023-01-06', 'SPL', 'U0004', 0),
(527, 'TR006', '2023-01-08', 'AC', 'U0004', 1);

-- --------------------------------------------------------

-- Table structure for table `ticket_passenger`

CREATE TABLE `ticket_passenger` (
  `ticket_id` int(11) NOT NULL,
  `no_of_passenger` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table `ticket_passenger`

INSERT INTO `ticket_passenger` (`ticket_id`, `no_of_passenger`) VALUES
(527, 1),
(790, 1);

-- --------------------------------------------------------
-- Table structure for table `train_coach`


CREATE TABLE `train_coach` (
  `train_no` varchar(10) NOT NULL,
  `coach_type` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table `train_coach`

INSERT INTO `train_coach` (`train_no`, `coach_type`) VALUES
('TR001', 'AC'),
('TR002', 'NON-AC'),
('TR003', 'GENERAL'),
('TR004', 'First Class'),
('TR005', 'Second Class'),
('TR006', 'AC'),
('TR007', 'AC'),
('TR008', 'AC'),
('TR009', 'NON-AC'),
('TR010', 'GENERAL'),
('TR011', 'AC'),
('TR012', 'AC'),
('TR013', 'AC'),
('TR014', 'AC'),
('TR015', 'NON-AC'),
('TR016', 'AC'),
('TR017', 'AC'),
('TR018', 'NON-AC'),
('TR019', 'GENERAL'),
('TR020', 'GENERAL'),
('TR004', 'GENERAL'),
('TR004', 'AC'),
('TR002', 'AC'),
('TR002', 'GENERAL'),
('TR002', 'AC'),
('TR002', 'AC'),
('TR001', 'NON-AC'),
('TR001', 'AC'),
('TR001', 'GENERAL'),
('TR011', 'AC');

-- --------------------------------------------------------
-- Table structure for table `train_date`

CREATE TABLE `train_date` (
  `train_no` varchar(10) NOT NULL,
  `depDate` date NOT NULL,
  `arrTime` varchar(10) NOT NULL,
  `depTime` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table `train_date`
INSERT INTO `train_date` (`train_no`, `depDate`, `arrTime`, `depTime`) VALUES
('TR001', '2023-01-03', '12:30pm', '3:45pm'),
('TR002', '2023-01-02', '2:30pm', '2:10am'),
('TR003', '2023-01-04', '6:50pm', '4:15am'),
('TR004', '2023-01-04', '2:30pm', '5:40pm'),
('TR005', '2023-01-05', '11:15am', '3:27pm'),
('TR006', '2023-01-06', '1:00pm', '4:10pm'),
('TR007', '2023-01-07', '7:00pm', '4:42am'),
('TR008', '2023-01-11', '6:45am', '9:45am'),
('TR009', '2023-01-02', '9:10pm', '7:45am'),
('TR010', '2023-01-03', '4:12pm', '1:00pm'),
('TR011', '2023-01-04', '3:15pm', '4:45pm'),
('TR012', '2023-01-05', '5:40pm', '3:15am'),
('TR013', '2023-01-06', '6:45am', '4:28pm'),
('TR014', '2023-01-07', '9:10pm', '6:30am'),
('TR015', '2023-01-01', '7:17am', '11:24pm'),
('TR016', '2023-01-02', '6:50pm', '4:15am'),
('TR017', '2023-01-03', '8:40pm', '9:10am'),
('TR018', '2023-01-04', '5:30pm', '6:45am'),
('TR019', '2023-01-05', '1:00pm', '4:27pm'),
('TR020', '2023-01-06', '3:45pm', '10:40am'),
('TR002', '2023-01-06', '5:55pm', '8:30am'),
('TR002', '2023-01-04', '6:25pm', '3:25am'),
('TR004', '2023-01-01', '3:55pm', '4:25am'),
('TR004', '2020-11-07', '2:54am', '12:45pm'),
('TR004', '2023-01-02', '8:45pm', '5:50am');

-- --------------------------------------------------------
-- Table structure for table `train_details`


CREATE TABLE `train_details` (
  `tname` varchar(55) NOT NULL,
  `start` varchar(10) NOT NULL,
  `end` varchar(10) NOT NULL,
  `halts` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table `train_details`

INSERT INTO `train_details` (`tname`, `start`, `end`, `halts`) VALUES
('Cairo', 'S0003', 'S0001', 4),
('Aswan', 'S0001', 'S0002', 1),
('Giza', 'S0003', 'S0013', 4),
('Luxor', 'S0001', 'S0002', 8),
('Ismailia', 'S0001', 'S0005', 3),
('Tanta', 'S0004', 'S0001', 7),
('Assiut', 'S0010', 'S0011', 7),
('Beheira', 'S0008', 'S0001', 3),
('Bani Suef', 'S0008', 'S0001', 4),
('Bani Suef Express', 'S0008', 'S0006', 3),
('Cairo Passenger', 'S0008', 'S0003', 7),
('Giza Express', 'S0008', 'S0006', 7),
('Bani Suef Express2', 'S0001', 'S0009', 6),
('Beheira Express', 'S0007', 'S0001', 3),
('Tanta Express', 'S0014', 'S0001', 7),
('Salem Express', 'S0006', 'S0011', 8),
('Helwan Express', 'S0003', 'S0001', 0),
('Mansoura Express', 'S0011', 'S0012', 5),
('West Cairo', 'S0001', 'S0005', 4),
('North Cairo', 'S0001', 'S0006', 5);

-- --------------------------------------------------------

-- Table structure for table `train_info`


CREATE TABLE `train_info` (
  `train_no` varchar(10) NOT NULL,
  `tname` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- Dumping data for table `train_info`

INSERT INTO `train_info` (`train_no`, `tname`) VALUES
('TR001', 'Cairo Express'),
('TR002', 'Beheira'),
('TR003', 'Tanta Express'),
('TR004', 'Cairo Express'),
('TR005', 'Aswan'),
('TR006', 'Fayyoum Express'),
('TR007', 'Luxor'),
('TR008', 'Beheira'),
('TR009', 'Cairo Express'),
('TR010', 'Cairo Express'),
('TR011', 'Giza Express'),
('TR012', 'Cairo Express'),
('TR013', 'Cairo Express'),
('TR014', 'Tanta Express'),
('TR015', 'Cairo Express'),
('TR016', 'Cairo Express'),
('TR017', 'Cairo Express'),
('TR018', 'Cairo Express'),
('TR019', 'Cairo Express'),
('TR020', 'Giza Express');

-- --------------------------------------------------------

-- Table structure for table `user_details`

CREATE TABLE `user_details` (
  `user_id` varchar(10) NOT NULL,
  `username` varchar(35) NOT NULL,
  `password` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table `user_details`

INSERT INTO `user_details` (`user_id`, `username`, `password`) VALUES
('U0001', 'Rawan', 'password'),
('U0002', 'Hawary', 'password'),
('U0003', 'Adel', 'password'),
('U0004', 'Bola', 'password'),
('U0005', 'Rana', 'password');

-- --------------------------------------------------------
-- Table structure for table `user_phone`

CREATE TABLE `user_phone` (
  `user_id` varchar(10) NOT NULL,
  `mobile` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table `user_phone`

INSERT INTO `user_phone` (`user_id`, `mobile`) VALUES
('U0001', '01060525280'),
('U0001', '01000025280'),
('U0001', '01000066680'),
('U0002', '01000066677'),
('U0003', '9455555143'),
('U0004', '9055582232'),
('U0005', '9456856741');

-- Indexes for dumped tables

-- Indexes for table `coach_price`
--
ALTER TABLE `coach_price`
  ADD PRIMARY KEY (`coach_type`);

--
-- Indexes for table `passenger_details`
--
ALTER TABLE `passenger_details`
  ADD PRIMARY KEY (`passenger_id`);

--
-- Indexes for table `passenger_ticket`
--
ALTER TABLE `passenger_ticket`
  ADD PRIMARY KEY (`passenger_id`);

--
-- Indexes for table `station`
--
ALTER TABLE `station`
  ADD PRIMARY KEY (`station_id`);

--
-- Indexes for table `ticket_passenger`
--
ALTER TABLE `ticket_passenger`
  ADD PRIMARY KEY (`ticket_id`);

--
-- Indexes for table `train_details`
--
ALTER TABLE `train_details`
  ADD PRIMARY KEY (`tname`);

--
-- Indexes for table `train_info`
--
ALTER TABLE `train_info`
  ADD PRIMARY KEY (`train_no`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ticket_passenger`
--
ALTER TABLE `ticket_passenger`
  MODIFY `ticket_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=957;
COMMIT;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;